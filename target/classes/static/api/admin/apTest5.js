/**
 * 分页查询测试5列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/apTest5/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询测试5详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/apTest5/query/' + id,
        method: 'get'
    })
}

/**
 * 新增测试5
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/apTest5/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改测试5
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/apTest5/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除测试5
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/apTest5/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出测试5
 *
 * @param query
 * @returns {*}
 */
function exportTest5(query) {
    return requests({
        url: '/admin/apTest5/export',
        method: 'get',
        params: query
    })
}
