/**
 * 数据库表列表
 *
 * @param query
 * @returns {*}
 */
function tableList(query) {
    return requests({
        url: '/tool/table/tableList',
        method: 'post',
        data: query
    })
}

/**
 * 查询已导入表列表
 * @param query query
 * @returns {*}
 */
function genTableList(query) {
    return requests({
        url: '/tool/table/genTableList',
        method: 'post',
        data: query
    })
}


/**
 * 导入表结构
 * @param query query
 * @returns {*}
 */
function importTable(query) {
    return requests({
        url: '/tool/table/importTable',
        method: 'post',
        data: query
    })
}

/**
 * 详情接口
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/tool/table/detail/' + id,
        method: 'get'
    })
}


/**
 * 部署代码
 * @param query query
 * @returns {*}
 */
function deployGenCode(query) {
    return requests({
        url: '/tool/table/deploy',
        method: 'post',
        data: query
    })
}

