package com.mars.framework.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取代码生成相关配置
 *
 * @author mars
 */
@Data
@Component
@ConfigurationProperties(prefix = "gen")
public class GenConfig {
    /**
     * 作者
     */
    private String author;

    /**
     * 生成包路径
     */
    private String packageName;

    /**
     * 自动去除表前缀，默认是true
     */
    private Boolean autoRemovePre;

    /**
     * 表前缀(类名不会包含表前缀)
     */
    private String tablePrefix;

    /**
     * 生成代码保存的目录(临时目录)
     */
    private String generatorCodePath;
    /**
     * Java后台 后端项目代码存放的目录
     */
    private String serverCodeDir;
    /**
     * 前端的目录（前端项目代码存放的目录）
     */
    private String htmlCodeDir;


}
