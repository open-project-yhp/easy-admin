package com.mars.common.request.sys;

import com.mars.common.request.PageRequest;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 菜单查询DTO
 *
 * @author 源码字节-程序员Mars
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ResourceInfoRequest extends PageRequest {

    @ApiModelProperty(value = "类型")
    private Integer type;

    @ApiModelProperty(value = "内容")
    private String title;

    @ApiModelProperty(value = "手机号")
    private String mobile;



}
