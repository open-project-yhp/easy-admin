package com.mars.common.request.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 菜单修改DTO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class SysMenuUpdateRequest {

    @NotNull
    @ApiModelProperty(value = "ID")
    private Long id;

    @NotEmpty
    @ApiModelProperty(value = "菜单名称")
    private String menuName;

    @ApiModelProperty(value = "父ID")
    private Long parentId = 0L;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "URL")
    private String url;

    @ApiModelProperty(value = "权限标识")
    private String perms;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "备注")
    private String remark;


}
