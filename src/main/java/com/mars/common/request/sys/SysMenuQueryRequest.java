package com.mars.common.request.sys;

import com.mars.common.request.PageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 菜单查询DTO
 *
 * @author 源码字节-程序员Mars
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SysMenuQueryRequest extends PageRequest {

    @ApiModelProperty(value = "菜单名称")
    private String menuName;

}
