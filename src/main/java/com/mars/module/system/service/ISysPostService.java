package com.mars.module.system.service;


import com.mars.common.request.sys.SysPostRequest;
import com.mars.common.response.PageInfo;
import com.mars.module.system.entity.SysPost;

import java.util.List;

/**
 * 岗位接口管理
 *
 * @author 源码字节-程序员Mars
 */
public interface ISysPostService {

    /**
     * 添加岗位
     *
     * @param request request
     */
    void add(SysPostRequest request);

    /**
     * 更新岗位信息
     *
     * @param request 请求参数
     */
    void update(SysPostRequest request);

    /**
     * 删除岗位
     *
     * @param id id
     */
    void delete(Long id);

    /**
     * 分页查询
     *
     * @param request 请求参数
     */
    PageInfo<SysPost> pageList(SysPostRequest request);

    /**
     * 岗位列表
     *
     * @return List<SysPost>
     */
    List<SysPost> getList();

    /**
     * 详情接口
     *
     * @param id id
     * @return SysPost
     */
    SysPost detail(Long id);

}
