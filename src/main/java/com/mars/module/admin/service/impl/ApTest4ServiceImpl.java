package com.mars.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.request.ApTest4Request;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.mars.module.admin.mapper.ApTest4Mapper;
import org.springframework.beans.BeanUtils;
import com.mars.module.admin.entity.ApTest4;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.module.admin.service.IApTest4Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 测试4业务层处理
 *
 * @author mars
 * @date 2023-12-22
 */
@Slf4j
@Service
@AllArgsConstructor
public class ApTest4ServiceImpl implements IApTest4Service {

    private final ApTest4Mapper baseMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApTest4 add(ApTest4Request request) {
        ApTest4 entity = ApTest4.builder().build();
        BeanUtils.copyProperties(request, entity);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(Long id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(List<Long> ids) {
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(ApTest4Request request) {
        ApTest4 entity = ApTest4.builder().build();
        BeanUtils.copyProperties(request, entity);
        return baseMapper.updateById(entity) > 0 ;
    }

    @Override
    public ApTest4 getById(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    public PageInfo<ApTest4> pageList(ApTest4Request request) {
        Page<ApTest4> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<ApTest4> query = this.buildWrapper(request);
        IPage<ApTest4> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }


    @Override
    public List<ApTest4> list() {
        return baseMapper.selectList(null);
    }

    private LambdaQueryWrapper<ApTest4> buildWrapper(ApTest4Request param) {
        LambdaQueryWrapper<ApTest4> query = new LambdaQueryWrapper<>();
         if (StringUtils.isNotBlank(param.getName())){
               query.like(ApTest4::getName ,param.getName());
        }
         if (StringUtils.isNotBlank(param.getAge())){
               query.like(ApTest4::getAge ,param.getAge());
        }
         if (StringUtils.isNotBlank(param.getGender())){
               query.like(ApTest4::getGender ,param.getGender());
        }
        return query;
    }

}
