package com.mars.module.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import java.util.*;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.*;
import cn.afterturn.easypoi.excel.annotation.Excel;
import java.time.LocalDateTime;
import com.mars.module.system.entity.BaseEntity;

    /**
 * 通知公告对象 sys_notify
 *
 * @author mars
 * @date 2023-12-06
 */

@Data
@ApiModel(value = "通知公告对象")
@Builder
@Accessors(chain = true)
@TableName("sys_notify")
public class SysNotify extends BaseEntity {


    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 标题
     */
    @Excel(name = "标题")
    @ApiModelProperty(value = "标题")
    private String title;

    /**
     * 类型
     */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private Integer type;

    /**
     * 内容
     */
    @Excel(name = "内容")
    @ApiModelProperty(value = "内容")
    private String content;

    /**
     * 备注
     */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间" , width = 30, databaseFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT, value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    /**
     * 创建人名称
     */
    @TableField(fill = FieldFill.INSERT, value = "create_by_name")
    @ApiModelProperty(value = "创建人名称")
    private String createByName;

    /**
     * 修改时间
     */
    @Excel(name = "修改时间" , width = 30, databaseFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE, value = "update_time")
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    /**
     * 更新人名称
     */
    @TableField(fill = FieldFill.UPDATE, value = "create_by_name")
    @ApiModelProperty(value = "更新人名称")
    private String updateByName;
}
