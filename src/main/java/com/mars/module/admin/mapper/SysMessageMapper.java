package com.mars.module.admin.mapper;

import com.mars.module.admin.entity.SysMessage;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 消息Mapper接口
 *
 * @author mars
 * @date 2023-12-06
 */
public interface SysMessageMapper extends BasePlusMapper<SysMessage> {

}
